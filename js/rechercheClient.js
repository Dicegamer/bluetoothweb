
const dateRegex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
const perRegex = /^(\d{2}|[1-9])$/g;

var jsonUser = {};
var jsonCustomer = {};
var jsonDeals = {};
var json = {};


var sessionToken = "";

function slip(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function initDeleteUserButtons() {
    $(".del-button").click(function() {
        var row = this.closest("tr");
        ajaxDeleteUser($(row).find(".id-user").html());
        this.closest("tr").remove();
    });
}

function initDeleteDealButtons() {
    $(".del-button").click(function() {
        var row = this.closest("tr");
        ajaxDeleteDeal($(row).find(".id-user").html());
        this.closest("tr").remove();
    });
}

 function initAddDealButton() {
    $("#addDealBtn").click(function() {
        $("#inputRow").slideToggle(50);
        $(this).fadeToggle(50);
    });
    $("#sendDealBtn").click(function() {
        var regexOk = true;

        let m;
        if ((m = dateRegex.exec($("#addDateFin").val())) !== null) {}
        else {
            $("#addDateFin").toggleClass("incorect-input");
            regexOk = false;
        }

        if ((m = perRegex.exec($("#addReduc").val())) !== null) {}
        else {
            $("#addReduc").toggleClass("incorect-input");
            regexOk = false;
        }

        if (regexOk){
            $("#inputRow").slideToggle(50);
            var d =  new Date(Date.now());
            var deal = { "start_at": d.toISOString(), "end_at": new Date($("#addDateFin").val()).toISOString(), "description": $("#addReduc").val()+"%", "label": $("#addRayon").val() }

            $.ajax({
                url : 'http://93.118.34.190/private/bluetooth/api/deals/',
                type : 'POST', // Le type de la requête HTTP, ici devenu POST
                headers: {Authorization: "Token "+sessionToken},
                contentType : 'application/json',
                data : JSON.stringify(deal),
                dataType : 'json'
            }).done(function(data) {
                $("#userTable").html("");
                ajaxGetAllDeals();
                initAddDealButton();
            }).fail(function(xhr, textStatus) {
                if (xhr.status = 401){
                    $("#id01").css("display","block");
                }
            });
        }
        $("#addDealBtn").fadeToggle(50);
    })
}

function initAddUserButton() {
   $("#addDealBtn").click(function() {
       $("#inputRow").slideToggle(50);
       $(this).fadeToggle(50);
   });
   $("#sendDealBtn").click(function() {

       deal = {
            "username": $("#addRayon").val(),
            "email": $("#addRayon").val()+"@mail.com",
            "password": $("#addDateFin").val()
       }

       $.ajax({
           url : 'http://93.118.34.190/private/bluetooth/api/users/',
           type : 'POST', // Le type de la requête HTTP, ici devenu POST
           headers: {Authorization: "Token "+sessionToken},
           contentType : 'application/json',
           data : JSON.stringify(deal),
           dataType : 'json'
       }).done(function(data) {
           $("#userTable").html("");
           getUser();
           initAddUserButton();
       }).fail(function(xhr, textStatus) {
           if (xhr.status = 401){
               $("#id01").css("display","block");
           }
       });
       $("#addDealBtn").fadeToggle(50);
   })
}

function initAddCustomerButton() {
   $("#addDealBtn").click(function() {
       $("#inputRow").slideToggle(50);
       $(this).fadeToggle(50);
   });
   $("#sendDealBtn").click(function() {

       deal = {
          "email": $("#addDateFin").val(),
          "bluetooth_mac_address": $("#addBT").val(),
          "first_name": $("#addReduc").val(),
          "last_name": $("#addRayon").val(),
          "phone_number": $("#addMail").val()
      }

       $.ajax({
           url : 'http://93.118.34.190/private/bluetooth/api/customers/',
           type : 'POST', // Le type de la requête HTTP, ici devenu POST
           headers: {Authorization: "Token "+sessionToken},
           contentType : 'application/json',
           data : JSON.stringify(deal),
           dataType : 'json'
       }).done(function(data) {
           $("#userTable").html("");
           ajaxGetAllUser();
           initAddCustomerButton();
       }).fail(function(xhr, textStatus) {
           if (xhr.status = 401){
               $("#id01").css("display","block");
           }
       });
       $("#addDealBtn").fadeToggle(50);
   })
}


function initNavBar() {
    $("#clients").click(function() {
        if (!$(this).hasClass('active')) {
            $(".nav-link").removeClass("active");
            $(this).addClass('active');
            $("#pageTitle").html("Liste des clients");
            $("thead").html("<tr><th>Numéro Client</th><th>Nom</th><th>Prenom</th><th>Adresse</th><th>Telephone</th><th>IDtel</th><th><button id=\"addDealBtn\"  type=\"button\" class=\"btn btn-success\" title=\"Ajouter une promotion\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></button></th></tr>");
            $("tbody").html("");
            $("#userTable").append("<tr id=\"inputRow\" class=\"table-warning\" style=\"display:none;\"><td></td><td><input id=\"addRayon\" class=\"form-control\" type=\"text\" placeholder=\"Nom\"></input></td><td><input id=\"addReduc\" class=\"form-control\" type=\"text\" placeholder=\"Prénom\"></td><td><input id=\"addDateFin\" class=\"form-control\" type=\"text\" placeholder=\"Adresse Mail\"></td><td><input id=\"addMail\" class=\"form-control\" type=\"text\" placeholder=\"Adresse Mail\"></td><td><input id=\"addBT\" class=\"form-control\" type=\"text\" placeholder=\"MacBluetooth\"></td><td><button id=\"sendDealBtn\" style=\"padding: 0,1em;\"  type=\"button\" class=\"btn btn-success\" title=\"Envoyer promotion\"><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i></button></td></tr>");
            ajaxGetAllUser();
            initAddCustomerButton();
        }
    })

    $("#promotions").click(function() {
        if (!$(this).hasClass('active')) {
            $(".nav-link").removeClass("active")
            $(this).addClass('active')
            $("#pageTitle").html("Liste des promotions")
            $("thead").html("<tr><th>id</th><th>Rayon concerné</th><th>Reduction (en %)</th><th>Date fin promotion</th><th><button id=\"addDealBtn\"  type=\"button\" class=\"btn btn-success\" title=\"Ajouter une promotion\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></button></th></tr>");
            $("tbody").html("");
            $("#userTable").append("<tr id=\"inputRow\" class=\"table-warning\" style=\"display:none;\"><td></td><td><select id=\"addRayon\" class=\"form-control\"><option>Alimentaire</option><option>Vetement</option><option>Jardinage</option></select></td><td><input id=\"addReduc\" class=\"form-control\" type=\"text\" placeholder=\"Réduction (en %)\"></td><td><input id=\"addDateFin\" class=\"form-control\" type=\"text\" placeholder=\"jj/mm/aaaa\"></td><td><button id=\"sendDealBtn\" style=\"padding: 0,1em;\"  type=\"button\" class=\"btn btn-success\" title=\"Envoyer promotion\"><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i></button></td></tr>");
            ajaxGetAllDeals();
            initDeleteDealButtons();
            initAddDealButton();
        }
    })

    $("#sensor").click(function() {
        if (!$(this).hasClass('active')) {
            $(".nav-link").removeClass("active")
            $(this).addClass('active')
            $("#pageTitle").html("Liste des Antennes")
            $("thead").html("<tr><th>id</th><th>Rayon</th><th>Position X</th><th>Position Y</th><th><button id=\"addDealBtn\"  type=\"button\" class=\"btn btn-success\" title=\"Ajouter une promotion\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></button></th></tr>");
            $("tbody").html("");
            $("#userTable").append("<tr id=\"inputRow\" class=\"table-warning\" style=\"display:none;\"><td></td><td><input id=\"addRayon\" class=\"form-control\"  type=\"text\" placeholder=\"Rayon\"></input></td><td><input id=\"addReduc\" class=\"form-control\" type=\"text\" placeholder=\"Coordonée X\"></td><td><input id=\"addDateFin\" class=\"form-control\" type=\"text\" placeholder=\"Coordonée Y\"></td><td><button id=\"sendDealBtn\" style=\"padding: 0,1em;\"  type=\"button\" class=\"btn btn-success\" title=\"Envoyer promotion\"><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i></button></td></tr>");
            getSensor();
            //initAddSensorButton()
        }
    })

    $("#usersList").click(function() {
        if (!$(this).hasClass('active')) {
            $(".nav-link").removeClass("active")
            $(this).addClass('active')
            $("#pageTitle").html("Liste des Utilisateurs")
            $("thead").html("<tr><th>id</th><th>Nom d'utilisateur</th><th><button id=\"addDealBtn\"  type=\"button\" class=\"btn btn-success\" title=\"Ajouter une promotion\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></button></th></tr>");
            $("tbody").html("");
            $("#userTable").append("<tr id=\"inputRow\" class=\"table-warning\" style=\"display:none;\"><td></td><td><input id=\"addRayon\" class=\"form-control\" type=\"text\" placeholder=\"Nom d'utilisateur\"></input></td><td><input id=\"addDateFin\" class=\"form-control\" type=\"password\" placeholder=\"Mot de passe\"></td><td><button id=\"sendDealBtn\" style=\"padding: 0,1em;\"  type=\"button\" class=\"btn btn-success\" title=\"Envoyer promotion\"><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i></button></td></tr>");
            getUser();
            initAddUserButton();
        }
    })

}

$("#loginBtn").click(function functionName() {
    console.log($("#login").val());
});


$(document).ready(function() {

    $("#logBtn").click(function() {
        console.log($("#login").val()+":"+$("#passwd").val());
        sendAjaxRequest("token/", "Basic "+btoa($("#login").val()+":"+$("#passwd").val()));
    });

    sendAjaxRequest("customers/");

})

function ajaxDeleteUser(idUser) {
    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/customers/"+idUser,
        type: "DELETE",
        headers: {Authorization: "Token "+sessionToken}
    }).done(function(data) {
        console.log("okey");
    }).fail(function(xhr, textStatus) {
        console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}

function ajaxGetAllUser(){
    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/customers/",
        type: "GET",
        headers: {Authorization: "Token "+sessionToken}
    }).done(function(data) {
        var user = {}
        for (myVar of data.customers) {
            getUserById(myVar.id, user);
        }

        showAllUser(user,data);

    }).fail(function(xhr, textStatus) {
        console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}


async function showAllUser(user,data) {
    await slip(1000);
    for (myVar of data.customers){
        idd = myVar.id
        $("#userTable").append(Mustache.render("<tr><td class=\"id-user\">{{id}}</td><td>{{first_name}}</td><td>{{last_name}}</td><td>"+user[idd].email+"</td><td>"+user[myVar.id].phone_number+"</td><td>"+user[myVar.id].bluetooth_mac_address+"</td><td><button class=\"del-button\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></button></td></tr>", myVar));
    }
    initDeleteUserButtons();
}

function getUserById(id, user) {
    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/customers/"+id,
        type: "GET",
        headers: {Authorization: "Token "+sessionToken}
    }).done(function(data) {
        id = data .id
        user[id] = data;

    }).fail(function(xhr, textStatus) {
        console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}

function ajaxDeleteDeal(idUser) {
    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/deals/"+idUser,
        type: "DELETE",
        headers: {Authorization: "Token "+sessionToken}
    }).done(function(data) {
        console.log("okey");
    }).fail(function(xhr, textStatus) {
        console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}

function ajaxGetAllDeals(){
    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/deals/",
        type: "GET",
        headers: {Authorization: "Token "+sessionToken}
    }).done(function(data) {
        var deal = {}
        for (myVar of data.deals) {
            getDealById(myVar.id, deal);
        }

        showAllDeals(deal,data);

    }).fail(function(xhr, textStatus) {
        console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}

async function showAllDeals(deal,data) {
    await slip(1000);
    for (myVar of data.deals){
        idd = myVar.id
        $("#userTable").append(Mustache.render("<tr><td class=\"id-user\">{{id}}</td><td>{{label}}</td><td>"+deal[idd].description+"</td><td>{{end_at}}</td><td><button class=\"del-button\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></button></td></tr>", myVar));
    }
    initDeleteDealButtons();
}

function getDealById(id, deal) {
    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/deals/"+id,
        type: "GET",
        headers: {Authorization: "Token "+sessionToken}
    }).done(function(data) {
        id = data .id
        deal[id] = data;

    }).fail(function(xhr, textStatus) {
        console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}


function initDeleteSensorButtons() {
    $(".del-button").click(function() {
        var row = this.closest("tr");
        deleteSensor(($(row).find(".id-user").html()));
        this.closest("tr").remove();
    });
}

function deleteSensor(id){
    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/sensors/"+id,
        type: "DELETE",
        headers: {Authorization: "Token "+sessionToken}
    }).done(function(data) {
        console.log("okey");
    }).fail(function(xhr, textStatus) {
        //console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}

function getSensor(){
    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/sensors/",
        type: "GET",
        headers: {Authorization: "Token "+sessionToken}
    }).done(function(data) {
        for (myVar of data.sensors){
            $("#userTable").append(Mustache.render("<tr><td class=\"id-user\">{{id}}</td><td>{{radius}}</td><td>{{pos_x}}</td><td>{{pos_y}}</td><td><button class=\"del-button\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></button></td></tr>", myVar));
        }
        initDeleteSensorButtons();

    }).fail(function(xhr, textStatus) {
        console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}




function initDeleteUserButtons() {
    $(".del-button").click(function() {
        var row = this.closest("tr");
        deleteUser(($(row).find(".id-user").html()));
        this.closest("tr").remove();
    });
}

function deleteUser(id){
    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/users/"+id,
        type: "DELETE",
        headers: {Authorization: "Token "+sessionToken}
    }).done(function(data) {
        console.log("okey");
    }).fail(function(xhr, textStatus) {
        //console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}

function getUser(){
    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/users/",
        type: "GET",
        headers: {Authorization: "Token "+sessionToken}
    }).done(function(data) {
        for (myVar of data.users){
            $("#userTable").append(Mustache.render("<tr><td class=\"id-user\">{{id}}</td><td>{{username}}</td><td><button class=\"del-button\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></button></td></tr>", myVar));
        }
        initDeleteUserButtons();

    }).fail(function(xhr, textStatus) {
        console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}



function sendAjaxRequest(path, auth = ""){

    $.ajax({
        url: "http://93.118.34.190/private/bluetooth/api/"+ path,
        type: "GET",
        headers: {Authorization: auth}
    }).done(function(data) {
        $("#id01").css("display","none");
        //console.log(data.token);
        document.cookie = "token="+data.token
        console.log(document.cookie);
        initNavBar();
        sessionToken = data.token;

        $(".del-button").click(function() {
            var row = this.closest("tr");
            ajaxDeleteUser($(row).find(".id-user").html());
            this.closest("tr").remove();
        })

        ajaxGetAllUser();
        initAddCustomerButton();

    }).fail(function(xhr, textStatus) {
        console.log(auth);
        if (xhr.status = 401){
            $("#id01").css("display","block");
        }
    });
}
